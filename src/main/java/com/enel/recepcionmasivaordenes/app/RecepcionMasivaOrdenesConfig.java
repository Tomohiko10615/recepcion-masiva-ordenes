package com.enel.recepcionmasivaordenes.app;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RecepcionMasivaOrdenesConfig {

	@Bean(name = "app")
	RecepcionMasivaOrdenes registrarRecepcionMasivaOrdenes() {
		return new RecepcionMasivaOrdenes();
	}
}
