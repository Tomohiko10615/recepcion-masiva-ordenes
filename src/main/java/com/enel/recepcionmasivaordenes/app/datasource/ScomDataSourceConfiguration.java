package com.enel.recepcionmasivaordenes.app.datasource;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.enel.recepcionmasivaordenes.app.repository.scom", 
entityManagerFactoryRef = "scomEntityManagerFactory", transactionManagerRef = "scomTransactionManager")
public class ScomDataSourceConfiguration {

	@Primary
	@Bean(name = "scomDataSourceProperties")
	@ConfigurationProperties("postgres.datasource")
	public DataSourceProperties scomDataSourceProperties() {
		return new DataSourceProperties();
	}

	@Primary
	@Bean(name = "scomDataSource")
	@ConfigurationProperties("postgres.datasource.configuration")
	public DataSource scomDataSource(
			@Qualifier("scomDataSourceProperties") DataSourceProperties scomDataSourceProperties) {
		return scomDataSourceProperties.initializeDataSourceBuilder().type(HikariDataSource.class).build();
	}

	@Primary
	@Bean(name = "scomEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean scomEntityManagerFactory(
			EntityManagerFactoryBuilder scomEntityManagerFactoryBuilder,
			@Qualifier("scomDataSource") DataSource scomDataSource) {

		Map<String, String> scomJpaProperties = new HashMap<>();
		scomJpaProperties.put("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");

		return scomEntityManagerFactoryBuilder.dataSource(scomDataSource)
				.packages("com.enel.recepcionmasivaordenes.app.entity.scom").persistenceUnit("scomDataSource")
				.properties(scomJpaProperties).build();
	}

	@Primary
	@Bean(name = "scomTransactionManager")
	public PlatformTransactionManager scomTransactionManager(
			@Qualifier("scomEntityManagerFactory") EntityManagerFactory scomEntityManagerFactory) {

		return new JpaTransactionManager(scomEntityManagerFactory);
	}

}
