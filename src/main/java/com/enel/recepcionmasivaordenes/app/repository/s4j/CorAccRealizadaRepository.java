package com.enel.recepcionmasivaordenes.app.repository.s4j;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enel.recepcionmasivaordenes.app.dto.AccionRealizadaDTO;
import com.enel.recepcionmasivaordenes.app.entity.s4j.CorAccRealizada;

@Repository
public interface CorAccRealizadaRepository extends JpaRepository<CorAccRealizada, Long> {

	@Query(value="SELECT id_acc_realizada AS dIdAccRealizada, "
			+ "coalesce(cod_interno,'Realizado') AS sCodInternoAccion "
			+ "FROM cor_acc_realizada "
			+ "WHERE cod_acc_realizada = ?1 "
			+ "AND id_empresa = ?2", nativeQuery=true)
	AccionRealizadaDTO obtenerAccionRealizada(String codAccRealizada, Integer idEmpresa);

}
