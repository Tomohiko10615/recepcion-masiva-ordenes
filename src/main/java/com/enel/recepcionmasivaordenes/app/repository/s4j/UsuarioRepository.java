package com.enel.recepcionmasivaordenes.app.repository.s4j;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enel.recepcionmasivaordenes.app.entity.s4j.Usuario;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

	@Query(value="SELECT id_usuario "
			+ "FROM seg_usuario "
			+ "where username = ?", nativeQuery=true)
	Long obtenerUsuarioId(String username);
	
	public static final String SQL_GS_OBS_RECEPCION = "SELECT username || ' (' || alias || ')' "
			+ "FROM seg_usuario "
			+ "WHERE id_usuario = ?";
}
