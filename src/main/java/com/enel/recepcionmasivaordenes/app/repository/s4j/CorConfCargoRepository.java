package com.enel.recepcionmasivaordenes.app.repository.s4j;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enel.recepcionmasivaordenes.app.dto.CargoDTO;
import com.enel.recepcionmasivaordenes.app.entity.s4j.CorConfCargo;

@Repository
public interface CorConfCargoRepository extends JpaRepository<CorConfCargo, Long> {

	@Query(value="SELECT id_cargo as idCargo, "
			+ "id_valor as idCodValor, "
			+ "codigo_gart as aCodGart "
			+ "FROM cor_conf_cargo a "
			+ "WHERE id_tipo_orden = ?1 "
			+ "AND id_acc_realizada = ?2 "
			+ "AND id_sit_terreno = ?3 "
			+ "AND a.id_tipo_acom = ?4 "
			+ "AND id_fase = ?5 "
			+ "AND limite_inf_potencia <= ?6 "
			+ "And limite_sup_potencia >= ?6 "
			+ "And tapa_con_ranura = ?7 "
			+ "And id_tarifa_base = ?8 "
			+ "and (id_sector_tipico = ?9 "
			+ "OR id_sector_tipico is NULL) "
			+ "and rownum <2", nativeQuery=true)
	CargoDTO obtenerCargo(Long dIdTipoOrdenH, Long dIdAccRealizada, Long dIdSitTerr, Long idTipoAcometida, Long idFase,
			Integer potenciaContratada, String tapaRanura, Long idTarifa, Long sectorTipico);

	@Query(value="SELECT id_cargo as idCargo, "
			+ "id_valor as idCodValor, "
			+ "codigo_gart as aCodGart "
			+ "FROM cor_conf_cargo a "
			+ "WHERE id_tipo_orden = ?1 "
			+ "AND id_acc_realizada = ?2 "
			+ "AND id_sit_terreno = ?3 "
			+ "AND a.id_tipo_acom = ?4 "
			+ "AND id_fase = ?5 "
			+ "AND limite_inf_potencia <= ?6 "
			+ "And limite_sup_potencia >= ?6 "
			+ "AND nivel_inicio <= ?7 "
			+ "And nivel_fin >= ?7 "
			+ "And id_tarifa_base = ?8 "
			+ "and (id_sector_tipico = ?9 "
			+ "OR id_sector_tipico is NULL) "
			+ "and rownum <2", nativeQuery=true)
	CargoDTO obtenerCargoAux(Long dIdTipoOrdenH, Long dIdAccRealizada, Long dIdSitTerr, Long idTipoAcometida,
			Long idFase, Integer potenciaContratada, Integer dNivel, Long idTarifa, Long sectorTipico);
	
	public static final String SQL_FAC_COBRO_ADIC_INSERT = "insert into fac_cobro_adic "
			+ "("
			+ "ID_COBRO_ADIC "
			+ ",ID_CARGO "
			+ ",ID_EMPRESA "
			+ ",ID_ESQUEMA_PRECIO "
			+ ",TIPO "
			+ ",ESTADO "
			+ ",FEC_COTIZACION "
			+ ",NAME_TIPO_OP "
			+ ",ID_COMERCIAL "
			+ ",TYPE_COMERCIAL "
			+ ",id_moneda "
			+ ") "
			+ "values "
			+ "("
			+ "? "
			+ ",? "
			+ ",? "
			+ ",? "
			+ ",'CobroPorCorte' "
			+ ",'Pendiente' "
			+ ",to_date(?,'ddMMyyyy hh24:mi') "
			+ ",'CobroPorCorte' "
			+ ",? "
			+ ",'com.synapsis.synergia.nucleo.domain.interfaces.servicioelectrico.ServicioElectrico' "
			+ ",1 "
			+ ")";
	
	public static final String SQL_ID_COBRO_ADIC = "SELECT SQOPERACIONADICIONAL.nextval FROM DUAL";

}
