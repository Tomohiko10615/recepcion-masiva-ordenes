package com.enel.recepcionmasivaordenes.app.repository.scom;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enel.recepcionmasivaordenes.app.dto.CodigoErrorDTO;
import com.enel.recepcionmasivaordenes.app.entity.scom.ComParametros;

@Repository
public interface ComParametrosRepository extends JpaRepository<ComParametros, Long> {

	@Query(value = "SELECT VALOR_NUM"
			+ " FROM schscom.COM_PARAMETROS"
			+ "	WHERE SISTEMA = 'EORDER'"
			+ "	AND ENTIDAD = 'ESTADO_TRANSFERENCIA'"
			+ "	AND CODIGO = ?", nativeQuery = true)
	Long obtenerEstado(String codigo);
	
	@Query(value = "SELECT VALOR_ALF AS VALOR, DESCRIPCION"
    		+ " FROM schscom.COM_PARAMETROS"
    		+ " WHERE SISTEMA = 'EORDER'"
    		+ " AND ENTIDAD = 'ESTADO_ERRSYN'"
    		+ " AND CODIGO = ?", nativeQuery = true)
	CodigoErrorDTO obtenerEstadoError(String codigo);
	
}
