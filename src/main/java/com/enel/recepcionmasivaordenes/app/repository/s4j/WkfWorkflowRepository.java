package com.enel.recepcionmasivaordenes.app.repository.s4j;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enel.recepcionmasivaordenes.app.entity.s4j.WkfWorkflow;

@Repository
public interface WkfWorkflowRepository extends JpaRepository<WkfWorkflow, Long> {

	@Query(value="UPDATE wkf_workflow SET "
			+ "id_old_state = id_state, "
			+ "id_state = ?1 "
			+ "WHERE id_workflow = ?2", nativeQuery=true)
	void actualizarWorkflow(String idState, Long idWorkflow);

	public static final String SQL_WFK_WORKFLOW_ORDEN_UPDATE = "update wkf_workflow "
			+ "set id_old_state = id_state "
			+ ",id_state    = ? "
			+ "where id_workflow = ?";
	
	public static final String SQL_WFK_WORKFLOW_SE_UPDATE = "update wkf_workflow "
			+ "Set id_old_state = id_state "
			+ ",id_state = ? "
			+ "where id_workflow  = ?";
	
	
}
