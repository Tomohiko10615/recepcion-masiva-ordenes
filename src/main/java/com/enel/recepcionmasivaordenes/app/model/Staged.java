package com.enel.recepcionmasivaordenes.app.model;

import com.enel.recepcionmasivaordenes.app.entity.s4j.OrdObservacion;
import com.enel.recepcionmasivaordenes.app.entity.s4j.OrdOrden;
import com.enel.recepcionmasivaordenes.app.entity.s4j.WkfWorkflow;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Staged {

	private WkfWorkflow wkfWorkflowSE;
	private OrdOrden ordOrden;
	private WkfWorkflow wkfWorkflowOrd;
	private OrdObservacion ordObservacion;
}
