package com.enel.recepcionmasivaordenes.app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.recepcionmasivaordenes.app.dto.CargoDTO;
import com.enel.recepcionmasivaordenes.app.repository.s4j.CorConfCargoRepository;
import com.enel.recepcionmasivaordenes.app.service.CorConfCargoService;

@Service
public class CorConfCargoServiceImpl implements CorConfCargoService {

	@Autowired
	private CorConfCargoRepository corConfCargoRepository;
	
	@Override
	public CargoDTO obtenerCargo(Long dIdTipoOrdenH, Long dIdAccRealizada, Long dIdSitTerr, Long idTipoAcometida,
			Long idFase, Integer potenciaContratada, String tapaRanura, Long idTarifa, Long sectorTipico) {
		return corConfCargoRepository.obtenerCargo(dIdTipoOrdenH, dIdAccRealizada, dIdSitTerr, idTipoAcometida, idFase,
				potenciaContratada, tapaRanura, idTarifa, sectorTipico);
	}

	@Override
	public CargoDTO obtenerCargoAux(Long dIdTipoOrdenH, Long dIdAccRealizada, Long dIdSitTerr, Long idTipoAcometida,
			Long idFase, Integer potenciaContratada, Integer dNivel, Long idTarifa, Long sectorTipico) {
		return corConfCargoRepository.obtenerCargoAux(dIdTipoOrdenH, dIdAccRealizada, dIdSitTerr, idTipoAcometida, idFase,
				potenciaContratada, dNivel, idTarifa, sectorTipico);
	}

}
