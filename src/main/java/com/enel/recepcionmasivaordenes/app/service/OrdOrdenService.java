package com.enel.recepcionmasivaordenes.app.service;

import com.enel.recepcionmasivaordenes.app.dto.OrdenDTO;

public interface OrdOrdenService {

	OrdenDTO obtenerOrden(String fechaEjec, Long dIdTipoOrdenH, String numeroOrden);

	OrdenDTO obtenerOrden(String aCodTipoOrden, String numeroOrden);

	void actualizarOrdOrden(String fechaEjec, Long dIdOrden);

}
