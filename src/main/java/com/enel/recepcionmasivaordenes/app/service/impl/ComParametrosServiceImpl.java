package com.enel.recepcionmasivaordenes.app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.recepcionmasivaordenes.app.dto.CodigoErrorDTO;
import com.enel.recepcionmasivaordenes.app.repository.scom.ComParametrosRepository;
import com.enel.recepcionmasivaordenes.app.service.ComParametrosService;

@Service
public class ComParametrosServiceImpl implements ComParametrosService {

	@Autowired
	private ComParametrosRepository comParametrosRepository;
	
	@Override
	public Long obtenerEstado(String codigo) {
		return comParametrosRepository.obtenerEstado(codigo);
	}

	@Override
	public CodigoErrorDTO obtenerEstadoError(String codigo) {
		return comParametrosRepository.obtenerEstadoError(codigo);
	}

}
