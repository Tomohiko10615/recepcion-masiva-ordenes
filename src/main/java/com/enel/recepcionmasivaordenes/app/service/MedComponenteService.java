package com.enel.recepcionmasivaordenes.app.service;

import com.enel.recepcionmasivaordenes.app.dto.ComponenteAuxDTO;
import com.enel.recepcionmasivaordenes.app.dto.ComponenteDTO;

public interface MedComponenteService {

	ComponenteDTO obtenerComponente(Long dIdServicioH, String nroComponente, String codModelo, String codMarca);

	ComponenteAuxDTO obtenerComponenteAux(Long dIdServicioH);

}
