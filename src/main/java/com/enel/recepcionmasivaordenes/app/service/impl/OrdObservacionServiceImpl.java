package com.enel.recepcionmasivaordenes.app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.recepcionmasivaordenes.app.repository.s4j.OrdObservacionRepository;
import com.enel.recepcionmasivaordenes.app.service.OrdObservacionService;

@Service
public class OrdObservacionServiceImpl implements OrdObservacionService {

	@Autowired
	private OrdObservacionRepository ordObservacionRepository;
	
	@Override
	public Long obtenerIdObservacion() {
		return ordObservacionRepository.obtenerIdObservacion();
	}

}
