package com.enel.recepcionmasivaordenes.app.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.recepcionmasivaordenes.app.repository.s4j.UtilRepository;
import com.enel.recepcionmasivaordenes.app.service.UtilService;

@Service
public class UtilServiceImpl implements UtilService {

	@Autowired
	private UtilRepository utilRepository;
	
	@Override
	public Date obtenerFechaSistema() {
		return utilRepository.obtenerFechaSistema();
	}

}
