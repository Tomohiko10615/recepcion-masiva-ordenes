package com.enel.recepcionmasivaordenes.app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.recepcionmasivaordenes.app.dto.AccionRealizadaDTO;
import com.enel.recepcionmasivaordenes.app.repository.s4j.CorAccRealizadaRepository;
import com.enel.recepcionmasivaordenes.app.service.CorAccRealizadaService;

@Service
public class CorAccRealizadaServiceImpl implements CorAccRealizadaService {

	@Autowired
	private CorAccRealizadaRepository corAccRealizadaRepository;
	
	@Override
	public AccionRealizadaDTO obtenerAccionRealizada(String codAccRealizada, Integer idEmpresa) {
		return corAccRealizadaRepository.obtenerAccionRealizada(codAccRealizada, idEmpresa);
	}

}
