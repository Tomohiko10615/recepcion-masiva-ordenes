package com.enel.recepcionmasivaordenes.app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.recepcionmasivaordenes.app.repository.s4j.CorEstSrvElectricoRepository;
import com.enel.recepcionmasivaordenes.app.service.CorEstSrvElectricoService;

@Service
public class CorEstSrvElectricoServiceImpl implements CorEstSrvElectricoService {

	@Autowired
	private CorEstSrvElectricoRepository corEstSrvElectricoRepository;
	
	@Override
	public String obtenerEstado(String aEstadoSE, String sauxCodInterno, String sCodInternoAccion) {
		return corEstSrvElectricoRepository.obtenerEstado(aEstadoSE, sauxCodInterno, sCodInternoAccion);
	}

}
