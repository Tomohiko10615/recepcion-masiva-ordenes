package com.enel.recepcionmasivaordenes.app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.recepcionmasivaordenes.app.dto.SituacionTerrenoDTO;
import com.enel.recepcionmasivaordenes.app.repository.s4j.ComSitTerrenoRepository;
import com.enel.recepcionmasivaordenes.app.service.ComSitTerrenoService;

@Service
public class ComSitTerrenoServiceImpl implements ComSitTerrenoService {

	@Autowired
	private ComSitTerrenoRepository comSitTerrenoRepository;
	
	@Override
	public SituacionTerrenoDTO obtenerSituacionTerreno(String codSitTerr, Integer idEmpresa) {
		return comSitTerrenoRepository.obtenerSituacionTerreno(codSitTerr, idEmpresa);
	}

}
