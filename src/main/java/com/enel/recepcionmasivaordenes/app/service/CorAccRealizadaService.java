package com.enel.recepcionmasivaordenes.app.service;

import com.enel.recepcionmasivaordenes.app.dto.AccionRealizadaDTO;

public interface CorAccRealizadaService {

	AccionRealizadaDTO obtenerAccionRealizada(String codAccRealizada, Integer idEmpresa);

}
