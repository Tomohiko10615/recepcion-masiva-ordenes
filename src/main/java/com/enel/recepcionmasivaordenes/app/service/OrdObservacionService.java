package com.enel.recepcionmasivaordenes.app.service;

public interface OrdObservacionService {

	Long obtenerIdObservacion();

}
