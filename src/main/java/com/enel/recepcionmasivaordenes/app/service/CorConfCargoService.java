package com.enel.recepcionmasivaordenes.app.service;

import com.enel.recepcionmasivaordenes.app.dto.CargoDTO;

public interface CorConfCargoService {

	CargoDTO obtenerCargo(Long dIdTipoOrdenH, Long dIdAccRealizada, Long dIdSitTerr, Long idTipoAcometida, Long idFase,
			Integer potenciaContratada, String tapaRanura, Long idTarifa, Long sectorTipico);

	CargoDTO obtenerCargoAux(Long dIdTipoOrdenH, Long dIdAccRealizada, Long dIdSitTerr, Long idTipoAcometida,
			Long idFase, Integer potenciaContratada, Integer dNivel, Long idTarifa, Long sectorTipico);

}
