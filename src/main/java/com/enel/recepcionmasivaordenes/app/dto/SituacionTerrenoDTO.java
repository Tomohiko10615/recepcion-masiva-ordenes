package com.enel.recepcionmasivaordenes.app.dto;

public interface SituacionTerrenoDTO {

	Long getDIdSitTerr();
	String getSCodInternoSitTerr();
}
