package com.enel.recepcionmasivaordenes.app.dto;

public interface TipoOrdenDTO {

	String getSauxCodInterno();
	Long getDIdTipoOrdenH();
}
