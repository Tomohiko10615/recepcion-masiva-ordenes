package com.enel.recepcionmasivaordenes.app.dto;

public interface ServicioDTO {

	Long getDIdServicioH();
	Long getDSecMagnitud();
	Long getIdWorkflowSE();
	String getAEstadoSE();
}
