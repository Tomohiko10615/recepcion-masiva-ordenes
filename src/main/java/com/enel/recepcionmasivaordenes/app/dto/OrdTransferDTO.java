package com.enel.recepcionmasivaordenes.app.dto;

public interface OrdTransferDTO {

	Long getLIdOrdTransfer();
	Long getLNroRecepcion();
}
