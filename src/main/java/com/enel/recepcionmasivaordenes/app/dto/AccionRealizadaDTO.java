package com.enel.recepcionmasivaordenes.app.dto;

public interface AccionRealizadaDTO {

	Long getDIdAccRealizada();
	String getSCodInternoAccion();
}
