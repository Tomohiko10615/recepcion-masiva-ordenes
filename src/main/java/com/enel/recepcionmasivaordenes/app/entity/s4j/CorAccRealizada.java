package com.enel.recepcionmasivaordenes.app.entity.s4j;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "cor_acc_realizada")
public class CorAccRealizada {

	@Id
	@Column(name = "id_acc_realizada")
	private Long idAccRealizada;
	
	@Column(name = "cod_interno")
	private String codInterno;
	
	@Column(name = "cod_acc_realizada")
	private String codAccRealizada;
}
