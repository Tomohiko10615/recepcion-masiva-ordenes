package com.enel.recepcionmasivaordenes.app.entity.s4j;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "ord_tipo_orden")
public class OrdTipoOrden {
	
	@Id
	private Long id;
	
	@Column(name = "cod_interno")
	private String codInterno;
	
	@Column(name = "cod_tipo_orden")
	private String codTipoOrden;
}
