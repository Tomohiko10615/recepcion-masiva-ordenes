package com.enel.recepcionmasivaordenes.app.entity.s4j;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "com_sit_terreno")
public class ComSitTerreno {

	@Id
	@Column(name = "id_sit_terreno")
	private Long idSitTerreno;
	
	@Column(name = "cod_interno")
	private String codInterno;
}
