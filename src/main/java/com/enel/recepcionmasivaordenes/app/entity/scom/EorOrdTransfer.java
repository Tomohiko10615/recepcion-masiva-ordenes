package com.enel.recepcionmasivaordenes.app.entity.scom;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "eor_ord_transfer")
public class EorOrdTransfer {

	@Id
	@Column(name = "id_ord_transfer")
	private Long idOrdTransfer;
	
	@Column(name = "nro_recepciones")
	private Long nroRecepciones;
	
	@Column(name = "nro_orden_legacy")
	private String nroOrdenLegacy;
	
	@Column(name = "COD_TIPO_ORDEN_EORDER")
	private String codTipoOrdenEorder;
}
