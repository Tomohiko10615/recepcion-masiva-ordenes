package com.enel.recepcionmasivaordenes.app.entity.s4j;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "nuc_empresa")
public class NucEmpresa {

	@Id
	@Column(name = "id_empresa")
	private Integer idEmpresa;
	
	@Column(name = "cod_partition")
	private String codPartition;
}
