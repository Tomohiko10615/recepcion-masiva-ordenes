package com.enel.recepcionmasivaordenes.app.entity.s4j;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "nuc_servicio")
public class NucServicio {

	@Id
	@Column(name = "id_servicio")
	private Long idServicio;
	
	@Column(name = "nro_servicio")
	private Long nroServicio;
	
	private String tipo;
}
