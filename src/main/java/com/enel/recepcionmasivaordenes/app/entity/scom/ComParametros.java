package com.enel.recepcionmasivaordenes.app.entity.scom;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "com_parametros")
public class ComParametros {

	@Id
	private Long id;
	
	@Column(name = "valor_num")
	private String valorNum;
	
	@Column(name = "valor_alf")
	private String valorAlf;
	
	private String sistema;
	private String entidad;
	private String codigo;
	private String descripcion;
}
