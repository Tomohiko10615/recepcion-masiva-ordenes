package com.enel.recepcionmasivaordenes.app.entity.s4j;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "com_ejecutor")
public class ComEjecutor {

	@Id
	@Column(name = "id_ejecutor")
	private Long idEjecutor;
	
	@Column(name = "cod_ejecutor")
	private String codEjecutor;
}
