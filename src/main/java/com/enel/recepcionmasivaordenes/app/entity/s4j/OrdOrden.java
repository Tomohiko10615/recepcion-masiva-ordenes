package com.enel.recepcionmasivaordenes.app.entity.s4j;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "ord_orden")
public class OrdOrden {

	@Id
	private Long id;
	
	@Column(name = "id_workflow")
	private Long idWorkflow;
	
	@Column(name = "id_tipo_orden")
	private Long idTipoOrden;
	
	@Column(name = "nro_orden")
	private String nroOrden;
	
}
